# SpriteSheetPacker (customized for cocos2d-x) #

**This is a fork of the project: [https://bitbucket.org/madhatter13/spritesheetpacker](https://bitbucket.org/madhatter13/spritesheetpacker) which was originally based on [SpriteSheetPacker on Codeplex](https://spritesheetpacker.codeplex.com/)**

The only change I made is that i used the plist export patch from [here](https://spritesheetpacker.codeplex.com/SourceControl/list/patches) and recoded it to work with the new SpriteSheetPacker interface.