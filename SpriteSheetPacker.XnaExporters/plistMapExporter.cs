﻿#region MIT License

/*
 * Copyright (c) 2009 Nick Gravelyn (nick@gravelyn.com), Markus Ewald (cygon@nuclex.org)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a 
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 * 
 */

#endregion

using System.Collections.Generic;
using System.Drawing;
using System.IO;

using System.Linq;
using System;

using sspack;

namespace SpriteSheetPacker.XnaExporters
{
    // writes out an XML file ready to be put into a XNA Content project and get compiled as content.
    // this file can be loaded using Content.Load<Dictionary<string, Rectangle>> from inside the game.
    public class PlistMapExporter : sspack.IMapExporter
    {
        public string MapExtension
        {
            get { return "plist"; }
        }

        public void SetNames(IEnumerable<ImageDetail> mapDetails)
        {
            // Keep the default names
        }

        public void Save(ExporterDetail exporterDetail)
        {
            var sortedMap = exporterDetail.MapDetails.OrderBy(imageDetail => imageDetail.Name);
            using (StreamWriter writer = new StreamWriter(exporterDetail.MapFileName))
            {
                writer.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                writer.WriteLine("<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");                
                writer.WriteLine("<plist version=\"1.0\">");
                writer.WriteLine("\t<dict>");
                writer.WriteLine("\t\t<key>frames</key>");
                writer.WriteLine("\t\t<dict>");
                foreach (var entry in sortedMap)
                {
                    Rectangle r = entry.OutputRectange;
                    string name = entry.Name;                    
                    writer.WriteLine(string.Format("\t\t\t<key>{0}</key>", Path.GetFileNameWithoutExtension(entry.FullPath)));
                    writer.WriteLine("\t\t\t<dict>");
                    writer.WriteLine("\t\t\t\t<key>frame</key>");
                    writer.WriteLine(string.Format("\t\t\t\t<string>{{{{{0},{1}}},{{{2},{3}}}}}</string>",
                        r.Left,
                        r.Top,
                        r.Width,
                        r.Height));
                    writer.WriteLine("\t\t\t\t<key>offset</key>");
                    writer.WriteLine("\t\t\t\t<string>{0,0}</string>");
                    writer.WriteLine("\t\t\t\t<key>sourceColorRect</key>");
                    writer.WriteLine(string.Format("\t\t\t\t<string>{{{{{0},{1}}},{{{2},{3}}}}}</string>",
                        0,
                        0,
                        r.Width,
                        r.Height));
                    writer.WriteLine("\t\t\t\t<key>sourceSize</key>");
                    writer.WriteLine(string.Format("\t\t\t\t<string>{{{0},{1}}}</string>",
                        r.Width,
                        r.Height));                                                        
                    writer.WriteLine("\t\t\t</dict>");
                }
                writer.WriteLine("\t\t</dict>");
                writer.WriteLine("\t\t<key>metadata</key>");
                writer.WriteLine("\t\t<dict>");
                writer.WriteLine("\t\t\t<key>format</key>");
                writer.WriteLine("\t\t\t<integer>1</integer>");                                
                writer.WriteLine("\t\t</dict>");
                writer.WriteLine("\t</dict>");
                writer.WriteLine("</plist>");
            }
        }
    }
}
