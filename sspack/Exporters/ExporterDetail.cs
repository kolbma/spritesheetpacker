﻿using System.Collections.Generic;
using System.Drawing;

namespace sspack
{
    public class ExporterDetail
    {
        public string ImageFileName { get; private set; }
        public Bitmap ImageBitmap { get; private set; }
        public string MapFileName { get; private set; }
        public IEnumerable<ImageDetail> MapDetails { get; private set; }

        public ExporterDetail(string imageFileName, Bitmap imageBitmap, string mapFileName, IEnumerable<ImageDetail> mapDetails)
        {
            ImageFileName = imageFileName;
            ImageBitmap = imageBitmap;
            MapFileName = mapFileName;
            MapDetails = mapDetails;
        }
    }
}