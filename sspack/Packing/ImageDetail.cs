﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace sspack
{
    public class ImageDetail
    {
        public string FullPath { get; private set; }
        public string Name { get; set; }
        public Bitmap Bitmap { get; private set; }
        public Size OriginalSize { get; private set; }
        public Rectangle CropRectangle { get; private set; }
        public Point Location { get; set; }
        public bool IsRotated { get; set; }

        public Rectangle OutputRectange { get { return new Rectangle(Location, Bitmap.Size); } }

        public ImageDetail(string fileName)
        {
            FullPath = Path.GetFullPath(fileName);
            Name = Path.GetFileNameWithoutExtension(fileName);
            InitialiseBitmap((Bitmap)Image.FromFile(fileName));
        }

        public ImageDetail(ImageDetail imageDetail, int frameIndex)
        {
            FullPath = imageDetail.FullPath;
            Name = imageDetail.Name + "_#" + frameIndex.ToString("00");

            imageDetail.Bitmap.SelectActiveFrame(FrameDimension.Time, frameIndex);
            InitialiseBitmap(imageDetail.Bitmap.Clone(imageDetail.CropRectangle, imageDetail.Bitmap.PixelFormat));
        }

        private void InitialiseBitmap(Bitmap bitmap)
        {
            Bitmap = bitmap;
            OriginalSize = Bitmap.Size;
            CropRectangle = new Rectangle { Size = Bitmap.Size };
            Location = Point.Empty;
        }

        public void CropImage(Rectangle cropRectangle)
        {
            if (Bitmap.Size == cropRectangle.Size)
                return;
            CropRectangle = cropRectangle;
            Bitmap = Bitmap.Clone(cropRectangle, Bitmap.PixelFormat);
        }

        public void RotateImage()
        {
            Bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
        }
    }
}
