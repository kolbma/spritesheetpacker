﻿using System.Collections.Generic;

namespace sspack
{
    public class ImagePackOptions
    {
        /// <summary>The list of file paths of the images to be combined.</summary>
        public IEnumerable<string> ImageFiles { get; set; }

        /// <summary>Whether or not the output image must have a power of two size.</summary>
        public bool RequirePowerOfTwo { get; set; }

        /// <summary>Whether or not the output image must be a square.</summary>
        public bool RequireSquareImage { get; set; }

        /// <summary>The maximum width of the output image.</summary>
        public int MaximumWidth { get; set; }

        /// <summary>The maximum height of the output image.</summary>
        public int MaximumHeight { get; set; }

        /// <summary>The amount of blank space to insert in between individual images.</summary>
        public int ImagePadding { get; set; }

        /// <summary>Whether or not to generate the map dictionary.</summary>
        public bool GenerateMap { get; set; }

        /// <summary>Whether or not to autoCrop the images.</summary>
        public bool AutoCrop { get; set; }

        /// <summary>Whether or not to autoCrop the images all to the same size.</summary>
        public bool AutoCropSame { get; set; }

        /// <summary>Whether or not image rotation is allowed.</summary>
        public bool AllowRotation { get; set; }
    }
}
