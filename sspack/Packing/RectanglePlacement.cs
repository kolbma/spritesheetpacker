﻿using System.Drawing;

namespace sspack
{
    public class RectanglePlacement
    {
        public Point Location { get; private set; }
        public bool IsRotated { get; private set; }

        public RectanglePlacement()
        {
        }

        public RectanglePlacement(Point point, bool isRotated)
        {
            Location = point;
            IsRotated = isRotated;
        }
    }
}