﻿#region MIT License

/*
 * Copyright (c) 2009-2010 Nick Gravelyn (nick@gravelyn.com), Markus Ewald (cygon@nuclex.org)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a 
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 * 
 */

#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace sspack
{
	public enum FailCode
	{
		FailedParsingArguments = 1,
		ImageExporter,
		MapExporter,
		NoImages,
		ImageNameCollision,

		FailedToLoadImage,
		FailedToPackImage,
		FailedToCreateImage,
		FailedToSaveImage,
		FailedToSaveMap
	}

	public class Program
	{
		static int Main(string[] args)
		{
			return Launch(args);
		}

		public static int Launch(string[] args)
		{
			ProgramArguments arguments = ProgramArguments.Parse(args);

			if (arguments == null)
			{
				return (int)FailCode.FailedParsingArguments;
			}
			else
			{
				// make sure we have our list of exporters
				Exporters.Load();

				// try to find matching exporters
				IImageExporter imageExporter = null;
				IMapExporter mapExporter = null;

				string imageExtension = Path.GetExtension(arguments.image).Substring(1).ToLower();
				foreach (var exporter in Exporters.ImageExporters)
				{
					if (exporter.ImageExtension.ToLower() == imageExtension)
					{
						imageExporter = exporter;
						break;
					}
				}

				if (imageExporter == null)
				{
					Console.WriteLine("Failed to find exporters for specified image type.");
					return (int)FailCode.ImageExporter;
				}

				if (!string.IsNullOrEmpty(arguments.map))
				{
					string mapExtension = Path.GetExtension(arguments.map).Substring(1).ToLower();
					foreach (var exporter in Exporters.MapExporters)
					{
						if (exporter.MapExtension.ToLower() == mapExtension)
						{
							mapExporter = exporter;
							break;
						}
					}

					if (mapExporter == null)
					{
						Console.WriteLine("Failed to find exporters for specified map type.");
						return (int)FailCode.MapExporter;
					}
				}

				// compile a list of images
				List<string> images = new List<string>();
				FindImages(arguments, images);

				// make sure we found some images
				if (images.Count == 0)
				{
					Console.WriteLine("No images to pack.");
					return (int)FailCode.NoImages;
				}

                Console.WriteLine("{0} Total images found.", images.Count);
                
                // generate our output
				ImagePacker imagePacker = new ImagePacker();
				Bitmap outputImage;
				IEnumerable<ImageDetail> outputMap;

				// pack the image, generating a map only if desired
                var options = new ImagePackOptions
                    {
                        ImageFiles = images,
                        RequirePowerOfTwo = arguments.pow2,
                        RequireSquareImage = arguments.sqr,
                        MaximumWidth = arguments.mw,
                        MaximumHeight = arguments.mh,
                        ImagePadding = arguments.pad,
                        GenerateMap = (mapExporter != null),
                        AutoCrop = arguments.autocrop,
                        AutoCropSame = arguments.cropsame,
                        AllowRotation = arguments.AllowRotation,
                    };
				int result = imagePacker.PackImage(options, out outputImage, out outputMap);
				if (result != 0)
				{
					Console.WriteLine("There was an error making the image sheet.");
					return result;
				}

			    var mapDetails = outputMap as IList<ImageDetail> ?? outputMap.ToList();

                if (mapExporter != null)
                {
                    mapExporter.SetNames(mapDetails);

                    var names = mapDetails.Select(imageDetail => imageDetail.Name).ToList();
                    if (names.Count != names.Distinct().Count())
                    {
                        Console.WriteLine("Duplicate image names exist!");
                        return (int)FailCode.ImageNameCollision;
                    }
                }

				// try to save using our exporters
				try 
				{
					if (File.Exists(arguments.image))
						File.Delete(arguments.image);
					imageExporter.Save(arguments.image, outputImage);
				}
				catch (Exception e)
				{
					Console.WriteLine("Error saving file: " + e.Message);
					return (int)FailCode.FailedToSaveImage;
				}

				if (mapExporter != null)
				{
					try
					{
						if (File.Exists(arguments.map))
							File.Delete(arguments.map); 
						mapExporter.Save(new ExporterDetail(arguments.image, outputImage, arguments.map, mapDetails));
					}
					catch (Exception e)
					{
						Console.WriteLine("Error saving file: " + e.Message);
						return (int)FailCode.FailedToSaveMap;
					}
				}
			}

			return 0;
		}

		private static void FindImages(ProgramArguments arguments, List<string> images)
		{
			List<string> inputFiles = new List<string>();

			if (!string.IsNullOrEmpty(arguments.il))
			{
                var fileNames = File.ReadAllLines(arguments.il);
                inputFiles.AddRange(GetExpandedFileList(fileNames));
			}

			if (arguments.input != null)
			{
                inputFiles.AddRange(GetExpandedFileList(arguments.input));
			}

			foreach (var str in inputFiles)
			{
				if (MiscHelper.IsImageFile(str))
				{
					images.Add(str);
				}
			}
		}

	    private static IEnumerable<string> GetExpandedFileList(IEnumerable<string> fileNames)
	    {
	        var expandedList = new List<string>();
            foreach (var filename in fileNames)
	        {
                if (filename.Contains("*"))
                {
                    int lastSlash = filename.LastIndexOf(@"\");
                    string path = filename.Substring(0, lastSlash);
                    string filenameOnly = filename.Substring(lastSlash + 1);
                    Console.WriteLine("Searching path= {0} for {1}", path, filenameOnly);
                    string[] theFiles = System.IO.Directory.GetFiles(path, filenameOnly); //get all the files that match the filename given.
                    Console.WriteLine("{0} files found.", theFiles.Length);
                    expandedList.AddRange(theFiles);
                }
                else
                {
                    expandedList.Add(filename);
                }
            }
	        return expandedList;
	    }
	}
}
